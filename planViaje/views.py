from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponse

from .models import *
from .forms import *

def index(request):
	return render(request, 'planViaje/index.html')

def horarios(request):
	horarios = Horario.objects.all()
	return render (request, 'planViaje/horarios.html',{'horarios':horarios})

def horario(request, horario_id):
	horario = get_object_or_404(Horario, pk=horario_id)
	return render (request, 'planViaje/horario.html',{'horario':horario})

def guardar_horario(request):
	if request.method == 'POST':
		form = Form_guardar_horario(request.POST, request.FILES)
		if form.is_valid():
			data = form.cleaned_data
			horario = Horario(
				dia_desde = data['dia_desde'],
				dia_hasta = data['dia_hasta'],
				hora_desde = data['hora_desde'],
				hora_hasta = data['hora_hasta']				
				)
			horario.save()
			return redirect('horarios')
		else:
			return render(request,'planViaje/form_guardar_horario.html/', {'form': form})
	else:
		form = Form_guardar_horario()
		return render(request, 'planViaje/form_guardar_horario.html/', {'form': form})

def editar_horario(request, horario_id):
	horario = Horario.objects.get(pk=horario_id)

	if request.method == 'GET':
		form = Form_editar_horario(instance=horario)
	else:
		form = Form_guardar_horario(request.POST, request.FILES, instance=horario)
		if form.is_valid():			
			form.save()
			return redirect('horarios')
	return render (request, 'planViaje/form_editar_horario.html',{'form': form})

def eliminar_horario(request, horario_id):
	horario = Horario.objects.get(pk=horario_id)
	horario.delete()
	return redirect ('horarios')
