from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('horarios/', views.horarios, name='horarios'),
	path('horario/<int:horario_id>/', views.horario, name='horario'),
	path('guardar_horario/', views.guardar_horario, name='guardar_horario'),
	path('editar_horario/<int:horario_id>/', views.editar_horario, name='editar_horario'),
	path('eliminar_horario/<int:horario_id>/', views.eliminar_horario, name='eliminar_horario'),	
]