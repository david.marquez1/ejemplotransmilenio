from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Horario(models.Model):	
	dias_semana = [
    ('LU', 'Lunes'),
    ('MA', 'Martes'),
    ('MI', 'Miercoles'),
    ('JU', 'Jueves'),
    ('VI', 'Viernes'),
	('SA', 'Sabado'),
	('DO', 'Domingo'),
	]
	dia_desde = models.CharField(null=False, max_length=2, choices=dias_semana, default='LU')
	dia_hasta = models.CharField(null=False, max_length=2, choices=dias_semana, default='VI')
	hora_desde = models.CharField(null=False, max_length=5)
	hora_hasta = models.CharField(null=False, max_length=5)

	def __str__(self):
		return str(self.id)