from django import forms

from .models import *

from django.forms import ModelForm

class Form_guardar_horario(forms.ModelForm):
	class Meta:
		model = Horario
		fields = ['dia_desde', 'dia_hasta', 'hora_desde', 'hora_hasta']
		widgets = {
			'dia_desde': forms.Select(attrs={'class': 'form-control'}),			
			'dia_hasta': forms.Select(attrs={'class': 'form-control'}),
			'hora_desde': forms.TextInput(attrs={'class': 'form-control'}),
			'hora_hasta': forms.TextInput(attrs={'class': 'form-control'}),
		}

class Form_editar_horario(forms.ModelForm):
	class Meta:
		model = Horario
		fields = ['dia_desde', 'dia_hasta', 'hora_desde', 'hora_hasta']
		widgets = {
			'dia_desde': forms.Select(attrs={'class': 'form-control'}),			
			'dia_hasta': forms.Select(attrs={'class': 'form-control'}),
			'hora_desde': forms.TextInput(attrs={'class': 'form-control'}),
			'hora_hasta': forms.TextInput(attrs={'class': 'form-control'}),
		}