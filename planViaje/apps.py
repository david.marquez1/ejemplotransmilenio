from django.apps import AppConfig


class PlanviajeConfig(AppConfig):
    name = 'planViaje'
